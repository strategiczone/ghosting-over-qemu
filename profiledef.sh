#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="ghosting_over_qemu"
iso_label="ARCH_$(date +%Y%m)"
iso_publisher="Strategic Zone"
iso_application="Arch Linux Live/Rescue CD"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'zstd' '-b' '1M' '-Xcompression-level' '15')
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/root/.ssh"]="0:0:0700"
  ["/root/.ssh/authorized_keys"]="0:0:0600"
  ["/usr/local/bin/start-qemu.sh"]="0:0:755"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/opt/qemu-ressources/noVNC"]="0:0:755"
  ["/opt/qemu-ressources/noVNC/utils/websockify"]="0:0:755"
  ["/opt/qemu-ressources/noVNC/utils/novnc_proxy"]="0:0:755"
)