#!/usr/bin/env bash

# Vars
# SYSDRIVE="/dev/nvme0n1"
SYSDRIVE=""
BRNAME="br0"
RANDOM_MAC1=($(printf 'FE:EE:C1:%02X:%02X:%02X\n' $[RANDOM%256] $[RANDOM%256] $[RANDOM%256]))
RANDOM_MAC2=($(printf 'FE:EE:C2:%02X:%02X:%02X\n' $[RANDOM%256] $[RANDOM%256] $[RANDOM%256]))

# Get the name and MAC of the first non-loopback NIC that is UP
INAME=$(ip -o link show up | grep -v ' lo' | awk '{print $2}' | sed 's/://' | head -n 1)
MAC_FILE="/tmp/original_mac_${INAME}.txt"

# Check if MAC file exists, if not save the REAL_MAC
if [ ! -f "$MAC_FILE" ]; then
    echo "Saving real MAC address to ${MAC_FILE}"
    REAL_MAC=$(cat /sys/class/net/${INAME}/address)
    echo "$REAL_MAC" > "$MAC_FILE"
else
    echo "Loading saved MAC address from ${MAC_FILE}"
    REAL_MAC=$(cat "$MAC_FILE")
fi

# Revert network changes on failure
cleanup_network() {
    echo "Cleaning up network..."

    # Remove bridge
    dhclient -r ${BRNAME} 
    ip link set ${BRNAME} down
    ip link delete ${BRNAME} type bridge

    # Reset original MAC to NIC
    if [ -f "$MAC_FILE" ]; then
        REAL_MAC=$(cat "$MAC_FILE")
        echo "Restoring original MAC address: ${REAL_MAC}"
        ip link set ${INAME} down
        ip link set ${INAME} address ${REAL_MAC}
        ip link set ${INAME} up
        rm -f "$MAC_FILE"  # Remove the temporary file
    fi
}

# Check if active network interface was found
if [ -z "$INAME" ] || ! ip link show "$INAME" > /dev/null 2>&1 ; then
    echo "Error: No active network interface found."
    exit 1
else
    echo "Using network interface: $INAME"
fi

# If SYSDRIVE is not provided (is empty), try to determine it dynamically
if [ -z "$SYSDRIVE" ]; then
    echo "No system drive specified. Attempting to detect the system drive..."
    for drive in nvme0n1 sda sdb sdc vda; do
        if [ -b "/dev/$drive" ]; then
            SYSDRIVE="/dev/$drive"
            echo "Found system drive: $SYSDRIVE"
            break
        fi
    done
fi

# Check if SYSDRIVE has been determined and is a block device
if [ -z "$SYSDRIVE" ] || [ ! -b "$SYSDRIVE" ]; then
    echo "Error: System drive $SYSDRIVE not found or is not a block device" >&2
    exit 1
else
    echo "Using system drive: $SYSDRIVE"
fi

# Create a bridge. Spoof real interface MAC address to VM & assign a random MAC to real interface and bridge
echo "Creating bridge ${BRNAME} and assigning MAC addresses..."
dhclient -r ${INAME}
ip link set ${INAME} down

ip link add ${BRNAME} type bridge
ip link set ${INAME} master ${BRNAME}

ip link set ${INAME} address ${RANDOM_MAC1}
ip link set ${INAME} up

ip link set ${BRNAME} address ${RANDOM_MAC2}
ip link set ${BRNAME} up

echo "Try to get IP from DHCP server for ${BRNAME}..."
dhclient -4 ${BRNAME}

# Check if IP address was assigned to bridge
if [ -z "$(ip -4 addr show ${BRNAME} | grep inet)" ]; then
    echo "Error: No IP address assigned to bridge ${BRNAME}"
    cleanup_network
    exit 1
else
    echo "IP address assigned to bridge ${BRNAME}"
fi

# Function to load KVM modules
load_kvm_modules() {
    echo "Loading KVM modules..."

    # Load the main KVM module
    if ! modprobe kvm; then
        echo "Error: Failed to load the 'kvm' module"
        return 1
    fi

    # Check CPU type and load the appropriate KVM module
    CPU_TYPE=$(grep -m 1 -o -E 'vmx|svm' /proc/cpuinfo | head -n 1)

    case "$CPU_TYPE" in
        vmx)
            MOD=kvm_intel
            ;;
        svm)
            MOD=kvm_amd
            ;;
        *)
            echo "Error: CPU does not support hardware virtualization or it is not enabled in BIOS/UEFI"
            return 1
            ;;
    esac

    # Attempt to load the CPU-specific KVM module
    if ! modprobe "$MOD"; then
        echo "Error: Failed to load the '$MOD' module"
        return 1
    fi
}

# Try to load KVM modules before starting QEMU
if ! load_kvm_modules; then
    echo "Error: Could not load KVM modules. Exiting"
    cleanup_network
    exit 1
fi

# Create a bridge config file
mkdir -p /etc/qemu
echo "allow all" | tee /etc/qemu/bridge.conf > /dev/null 2>&1
chmod 0640 /etc/qemu/bridge.conf


# Define a function to start QEMU
start_qemu() {
    echo "Starting Ghost VM..."

    # Check if 'quick_registration' is set in the kernel parameters
    quick_registration_value=""
    if grep -q "quick_registration" /proc/cmdline; then
        # Extract the value
        quick_registration_value=$(cat /proc/cmdline | tr ' ' '\n' | grep "quick_registration" | cut -d'=' -f2)
    fi

    # Interrupt the boot process to select the FOG menu entry if 'quick_registration' is not set
    if [ "$quick_registration_value" != "true" ]; then
        bash -c "
        sleep 12
        for i in {1..6};
        do
            echo sendkey down | nc -N 127.0.0.1 55555
            sleep 0.5
        done" &
    fi

    # Run QEMU and redirect its output to a specific log file
    qemu-system-x86_64 \
        -drive "if=pflash,format=raw,readonly=on,file=/opt/qemu-ressources/OVMF_CODE.fd" \
        -drive "if=pflash,format=raw,readonly=on,file=/opt/qemu-ressources/OVMF_VARS.fd" \
        -enable-kvm \
        -cpu host \
        -m 6G \
        -rtc base=utc \
        -net nic,model=virtio,macaddr=${REAL_MAC} \
        -net bridge,br=${BRNAME},id=net0 \
        -hda ${SYSDRIVE} \
        -vga virtio \
        -cdrom /opt/qemu-ressources/netboot.iso \
        -display sdl,grab-mod=lshift-lctrl-lalt,window-close=off \
        -monitor tcp:127.0.0.1:55555,server,nowait
}

# Call the start_qemu function
if ! start_qemu; then
    echo "Error: Failed to start QEMU."
    cleanup_network
    exit 1
fi
