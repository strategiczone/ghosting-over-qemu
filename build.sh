#!/usr/bin/env bash
# shellcheck disable=SC2034
if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi


# Define variables
ARCHISO_DIR="/tmp/archiso"
WORK_DIR="work_dir"
OUT_DIR="out_dir"

# Install packages
pacman -Syu --noconfirm archiso reflector rsync rclone curl pwgen git nftables

# Clone noVNC repository to airootfs
git clone https://github.com/novnc/noVNC.git --depth 1 airootfs/opt/qemu-ressources/noVNC

# Create symbolic links for systemd services
ln -sf /usr/lib/systemd/system/nftables.service $ARCHISO_DIR/$WORK_DIR/airootfs/etc/systemd/system/

# Build the Ghosting over QEMU ISO
rm -rf $ARCHISO_DIR
mkdir -p $ARCHISO_DIR/{$WORK_DIR,$OUT_DIR}

# Temporary fix for https://gitlab.archlinux.org/archlinux/archiso/-/issues/225
mkdir -p $ARCHISO_DIR/$WORK_DIR/x86_64/airootfs/usr/share/licenses/common/GPL2
cp /usr/share/licenses/spdx/GPL-2.0-only.txt $ARCHISO_DIR/$WORK_DIR/x86_64/airootfs/usr/share/licenses/common/GPL2/license.txt

mkarchiso -A "Ghosting over QEMU" -P "Strategic Zone" -v -w $ARCHISO_DIR/$WORK_DIR -o $ARCHISO_DIR/$OUT_DIR ./
